const dotenv = require('dotenv').config();
const fs = require('fs');
const readline = require('readline');
const jsforce = require('jsforce');
const moment = require('moment');
const json = require('json-keys-sort'); 

// Salesforce org login credentials
const username = process.env.SALESFORCE_USERNAME;
const password = process.env.SALESFORCE_PASSWORD;
const securityToken = process.env.SALESFORCE_SECURITY_TOKEN;

// channel name of the event
const eventName = 'Event__e';

function generateFileName() {
  return eventName + moment().format('YYYYMMDD-HHmmss')
};

function writeEventMsgToFile(message) {
  !fs.existsSync(`./messages/`) && fs.mkdirSync(`./messages/`, { recursive: true })
  fs.writeFile(`./messages/${generateFileName()}.json`, JSON.stringify(json.sort(message), null, 4), function (err) {
    if (err) throw err;
  });
}

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);

const conn = new jsforce.Connection({
  loginUrl: process.env.SALESFORCE_LOGIN_URL,
  version: process.env.SALESFORCE_API_VERSION
});

conn.login(username, password + securityToken, (err, userInfo) => {
  if (err) { return console.error(err); }

  console.log('Authentication succeeded');

  conn.streaming.topic('/event/' + eventName).subscribe((message) => {
    console.log(message);
    writeEventMsgToFile(message);
    console.log('Press any key to exit...');
  });
});

process.stdin.on('keypress', (str, key) => {
    conn.logout((err) => {
      if (err) { return console.error(err); }
    });
    process.exit();
});