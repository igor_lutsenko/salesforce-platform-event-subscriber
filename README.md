# Salesforce Platform Event Subscriber

A simple application for subscribing to Salsesforce platform events with saving received messages.

## Requirements

* Node >= 8

## Usage

Install all dependencies

```
npm install
```

Enter your org credentials into an `.env` file.

* SALESFORCE_LOGIN_URL=`https://test.salesforce.com`
* SALESFORCE_USERNAME=`your_username`
* SALESFORCE_PASSWORD=`your_password`
* SALESFORCE_SECURITY_TOKEN=`your_security_token`
* SALESFORCE_API_VERSION=`51.0`

In `app.js` specify desired channel name of the event.

```javascript
// channel name of the event
const eventName = 'Event__e';
```

Run the app

```
npm start
```

The application will be authenticated, subscribe to the channel and wait for messages.

Each received message will be saved to a separate json file in the `/messages` directory.
